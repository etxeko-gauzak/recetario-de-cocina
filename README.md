# Recetario de Cocina

Listado de recetas de cocina.

# Índice
* [Salados](#salados)
  1. [Salsa de Tomate](#salsa-de-tomate)
  2. [Salsa Rubia](#salsa-rubia)
  3. [Champiñones en salsa con huevo](#champi%C3%B1ones-en-salsa-con-huevo-karlos-argui%C3%B1ano)
  4. [Oilaskoa Aitaren Modura](#oilaskoa-aitaren-modura)
  5. [Revuelto con Foie + Patatas + Jamón](#revuelto-con-foiepatatasjam%C3%B3n)
  6. [Arroz Kubak](#arroz-kubak-arroz-crujiente-con-gamas-y-verduras-kubak)
  7. [Pan de Naan](#pan-de-naan)
  8. [Empanada de Atún](#empanada-de-atún)
  9. [Alcachofas](#alcachofas)
  
* [Postres](#postres)
  1. [Brownie de Chocolate](#brownie-de-chocolate)
  2. [Tarta de 3 Chocolates](#tarta-de-3-chocolates)
  3. [Donuts](#donuts)
  4. [Tarta de Chocolate de Izeko](#tarta-de-chocolate-de-izeko)

# SALADOS

## Salsa de Tomate

* **Ingredientes**:
  * Una lata de tomate triturado ó en enteros
  * Una cebolleta y media ó en caso de tener una cebolla grande normal, utilizar media ó algo menos todo es en función de si haces una lata de 1 Kg ó de 250 gr.
  * Un diente de ajo (no una cabeza)
  * Aceite (Cantidad suficiente para que cubra el fondo de la cazuela que vayas a emplear). 

* **Elaboración**:
  1) Se calienta el aceite
  2) Se echa la cebolla y se sofríe un poco que empiece a ablandarse,un poco dorado junto con el ajo.
  3) Seguido se echa el tomate.
  4) Cuando está un ratito "plo plo plo" échale un poco de azúcar (la punta de una cucharilla ó puedes echarle algo más ya sabes que a tí te gusta más echando para dulce que ácido y por supuesto que seguido le echas la sal.Yo la sal cojo con los dedos y va a ojo.De todas formas lo mismo cuando eches el azúcar y eches la sal lo mejor hasta coger el "tino" lo vas probando.
  5) Hay que tener un buen rato cociendo ploploplo para que se evapore todo el agua y quede gordito.No lo hagas a fuego fuerte y lo tapas porque el tomate salta mucho.De vez en cuando lo destapas y lo vas removiendo para que no se pegue en el cazo.Hay que estar cerca del cazo por lo que te digo de remover y no te vayas a hacer otra cosa lejos del cazo.
  6) Una vez que lo ves gordito lo pasas por el pasa purés y listo  

<a href="#top">Volver</a>

## Salsa Rubia

* **Ingredientes (para 4 personas)**:
  *  1 Cebolla
  *  Harina
  *  Vino blanco / Cognac
  *  1/2 pastilla Avecrem

* **Elaboración**:
  1) Picamos la cebolla mucho
  2) La ponemos al fuego hasta que dore (mucho)
  3) Cuando esté dorada le añadimos la harina (cucharada rasa), le añadimos el chorretón de vino blanco ó cognac y la media pastilla de Avecrem.
  4) Lo dejamos al fuego 12 minutos hasta que haga "plo-plo" y si vemos que engorda mucho le añadimos agua.
  5) Por último lo pasamos por el colador.

## Champiñones en salsa con huevo (Karlos Arguiñano)

* **Ingredientes**:
  * 1 Vaso de vino tinto o blanco.
  * 2 Cucharadas de aceite.
  * 4 o 5 dientes de ajo picados.
  * 1 Cebolla entera picada.
  * 1 Cayena (guindillitas rojas).
  * 1 Bandeja de champiñones.
  * 1 Cucharada de harina.

* **Elaboración**:
  1) Pochar la cebolla con los ajos y la guindilla. 
  2) Cuando está pochado añadir una cucharada de harina. 
  3) Rehogar y añadir un vaso de vino blanco o vino tinto. Si usamos un vaso de agua, sería hasta la mitad de vino.
  4) Por otro lado tienes que tener cortado los champiñones en laminas (o usar una bandeja de champiñones laminados).
  5) Después de lo del vino se añaden los champiñones. 
  6) Rehogar e ir cociendo de 12 a 15 minutos a fuego no muy fuerte, plo, plo ,plo. No hace falta añadir agua porque el champiñón suelta su agua y ya es suficiente. 
  7) Cuando ya lo tienes todo blandito y ha pasado el tiempo de la coción se pone sobre una bandeja para horno y encima se cascan dos huevos y se pone el horno a 200ºC-210ºC durante 4 o 5 minutos, que quedan bien cuajados.

* **Referencia**: 
  * http://www.hogarutil.com/Cocina/Programas+de+Televisi%C3%B3n/K.+Argui%C3%B1ano+en+tu+cocina/Recetas+anteriores/Champi%C3%B1ones+en+salsa+con+huevo

<a href="#top">Volver</a>

## Oilaskoa Aitaren modura

Errezeta hau orain dala urte asko (hobe ez esatea zenbat) errebista batetik hartu nuen.
Jatorriko errezeta, “Martsellar erara” esaten zan, uste dut. Baina aldaketa batzuk egin ondoren (hobetzeko, noski), “nire erara” bihurtu zan.

* **Osagaiak**:
  * Oilasko bat
  * Sagarlurrak, zuk gura dozun kopurua. Azkenean honena da.
  * Txerri koipea, 250 g (Tarrina bat), bederen. Hobeto bi.
  * Olioa.
  * Limoi bat. (Bere zukua erabiltzen da). Bi erabiltzen badozu, etzara pasatsen.
  * Piperbaltza , zenbat eta gehiago, zapore hobeagoa. Pasatu barik, noski. 20 edo 30 aleak, beharbada nahikoa da.
  * Baratxuriak, 5 edo 10. Zatituak erabiliko dira, baina ez zati txikiegitan.
  * Gatza.
  * Koinak arrunta, gustura. Amieran saporea ez da nabaritzen.
  * Piperminak (guindilla) bi edo hiru.
  * Piperbaltza zaporea emon da. Pipermina “puntu mina” emoten da.
 
* **Nola egin**:
  1) Oilasko garbia, koipeztatzen da, barrutik eta kanpotik.
  2) Gatza, limoia, koinaka, baratxuriren bat, zatitua, dana barrutik eta kanpotik.
  3) Patatak zati txikitan zatitzen dira. Zukaldarien modura, ez moztuta: moztu eta apurtu.
  4) Piperbaltza zatitzen da. Beste modurik ez ba dozu, botila bategaz miatu daiteke. Heren bat oilasko barruan, bestea oilasko kanpotik eta patatakaz. Horregaitik ale asko izan beharrezkoak dira.
  5) Piperminak zatitzen dira. Eskuz egiten baduzu, gero atzamarrak garbitu. Zati batzuk oilasko barruan, bestea patatakaz.
  6) Txarri koipea, patatak, beste gauzak eta oilaskoaren barruan ez dagona, erretiluan (labeko bandeja) ipintzen da. Oilaskogaz, noski. Koinaka eransten da oilasko gainean.
  7) Olioa ere eransten da, patata legorrean ez geratzeko.
  8) Labean sartzen da eta noizbehinka begiratzen da, eta bueltaren bat egiten da danari: oilaskoari eta patatei. Bestela, edo oilasko erretzen da edo patatak guztiz legorrak geratuko dira.

Horregaitik nik sarritan txarri koipea “gehiegi” ipintzen dut: patatak estaltzeko.
Eta horregaitik beti koipe asko soberatzen da. Koipe honegaz patata gehiago egin daiteke.

**Oharrak**: 
  * Okelak izaten diranean, labearen tenperatura 250 º, bederen, izan behar da.
  * Nola doan jakiteko, noizean beinka patatak eta oilaskoa ziztatu.
  * Labeko erretilua sakona izan beharko zen, patatak ondo estalita gera daitezen. Bestela sarriago bira egin beharko dozu patatak, ez legortzeko
  * Lehenbizi oilaskoa ondo erreta geratuko da. Horregaitik patatak apurtxu bat gehiago geratu beharko dira. Guztietara, ordu bat t’erdi edo ordu bi pentsatu beharko dozue.

  <a href="#top">Volver</a>
  
## Revuelto con Foie+Patatas+Jamón

**Importante**: Esta receta se realiza en el Tupperware rojo (con el que hago los brownies de microondas).

* **Ingredientes**:
  * 2 x Patatas
  * 1/4 Cebolla (si voy a echar cebolleta, 2)
  * 2 x Huevo
  * 1/2 tarrina de Foie (de formato trapezoidal)
  * Jamón (es para recubrir una vez terminado el plato)
  * Aceite (chorretón)
  * Sal (pizca o mijita)
  * Vinagre balsámico de Modena (el espeso) (es para adornar una vez terminado el plato) 
   
* **Elaboración**:
  1) Cortamos las patatas en "bastón" pero algo más finas de lo normal. 
  2) Las echamos en el Tupperware con sal y aceite y a máxima potencia metermos en el mircroondas durante 5 minutos. 
  3) A continuación lo sacamos, lo removemos y lo volvemos a meter 5 minutos más.
  4) Por otro lado, pochamos la cebolla en una sarten a fuego lento y con poco aceite. 
  5) Si queremos caramelizar la cebolla, echamos una cucharadita de azucar y lo seguimos pochando.
  6) Después de los 5 minutos del Tupperware (los segundos 5 mins), sacamos del microondas y echamos los 2 huevos (sin batir), la cebolla y el foie cortadito. Lo removemos y lo volvemos a meter en el microondas durante 2 minutos. A continuación lo sacamos, y en función de como esté hecho, lo volvemos a meter o no.

A la hora de servir, añadimos el jamón y el vinagre balsámico de Modena (a modo de adorno). 

  <a href="#top">Volver</a>

## Arroz Kubak (Arroz crujiente con gamas y verduras Kubak)

* **Ingredientes**:
  *  1 vaso de arroz grano largo 
  *  1 1⁄2 vaso de agua 
  *  3 vasos de aceite de girasol para freír el arroz 
  *  300 gramos de gambas frescas o congeladas 
  *  2 cucharadas soperas de salsa de soja 
  *  4 cucharaditas de harina 
  *  1 cucharada sopera de vinagre blanco 
  *  2 cucharaditas de azúcar 
  *  1⁄2 pastilla de avecrem ave 
  *  1 diente de ajo picado 
  *  1 pimiento verde troceado fino 
  *  4 cebollas troceadas finas 
  *  3 zanahorias medianas cortadas finas 
  *  2 vasos de guisantes congelados o en lata 

* **Elaboración**:
  1) Lava en abundante agua un vaso de arroz de grano largo o grano medio. Añade en una olla, un vaso y medio de agua (o un vaso y 1⁄4 si se trata de grano medio) y  añade también el arroz. 
  2) Tapar la olla y cocer a fuego lento el arroz durante 30 minutos. Retirar del fuego y mientras lo dejamos enfriar precalentamos el horno a 150o C. 
  3) Vierte el arroz en la bandeja del horno que estará ligeramente engrasada asegurándote que la capa de arroz no supera los 0,70cm de espesor. Hornea el arroz en posición media del horno de 50 a 55 minutos hasta que quede bien seco y dale la vuelta si es necesario para que se sequen ambos lados. 
  4) Una vez fuera del horno déjalo enfriar un rato y después corta el arroz en piezas de 5 por 5cm. Puedes guardar los trozos de arroz en un recipiente en la nevera, pero no los congeles. 
  5) Mientras tanto descongela las gambas si son congeladas y mezcla en un bol las dos cucharadas de salsa de soja las cuatro cucharaditas de harina, la cucharada de vinagre, las dos cucharaditas de azúcar, un vaso de agua y media pastilla de avecrem de ave. Déjalo reposar. 
  6) Ya tienes el arroz preparado para freírlo. El truco del arroz crujiente consiste en que tanto el arroz como la salsa que lo acompañan estén muy calientes ambos. 
  7) Por tanto freímos en abundante aceite los trozos de arroz a 185o C hasta que el arroz quede ligeramente dorado y se hinche, durante no más de 2 minutos. Servimos el arroz en una bandeja y lo secamos con papel de cocina. Justo antes de servir, verter la salsa, que debe estar muy caliente, sobre el arroz. Escucharás el arroz chisporrotear. 
  8) Un truco: si no está bien cocinado, en el arroz puede crecer una bacteria llamada bacilo del cereal, así que se recomienda que el arroz esté completamente seco antes de enfriarlo y luego cortarlo en trozos como se indica arriba. Estos trozos de arroz horneado se pueden refrigerar en la nevera pero no es recomendable comerlos si llevan más de 7 días en la misma. 

  9) **Salsa**: 
     Pon una cucharada o dos de aceite de girasol en una sartén a fuego medio alto. Fríe y revuelve con una cuchara de palo el ajo durante 20 segundos, añade el pimiento y las cebollas y fríelas removiendo durante 1 minuto y medio. Apártalo de la sartén en un plato. Añade las zanahorias y fríelas durante 3 minutos, añade los guisantes y fríe durante 1 minuto. Apártalo de la sartén en el mismo plato. Añade las gambas y fríelas hasta que estén rosadas y luego el caldo de soja. Cocina la salsa hasta que espese y salgan burbujas. Añade la verdura a la sartén y posteriormente viértelo encima del arroz crujiente. Y a comer!!! Que aproveche. 
     
  <a href="#top">Volver</a>

## Pan de Naan (Indio)

* **Ingredientes**:
  * 100 cc. agua templada
  * 1 yogurt natural sin azúcar (125 gr.)
  * 300 gr. harina de fuerza
  * 1 diente de ajo (pelado y picado muy fino)
  * 1 cucharadita de sal
  * 2 cucharaditas de miel
  * 1 cucharada de mantequilla (o aceite)
  * 1 cucharadita de levadura seca de panadero (o 25g de levadura fresca)
  * 1 cucharadita de cominos
  * Mantequilla, para servir.
  * Aceite de girasol, para engrasar

* **Elaboración**:
  1) Introduce en el vaso de la thermomix el agua y el yogurt. 
  2) Cubre con la harina, el ajo picado. 
  3) Añade la miel, la sal y la mantequilla, en tres esquinas diferentes. 
  4) Deposita la levadura en un hueco hecho en el centro de la harina.
  5) Mezclar unos segundos a velocidad 6. 
  6) Amasar 3 minutos velocidad espiga.
  7) Pasa a un cuenco ligeramente aceitado, cubre con film y deja fermentar en sitio cálido, en la cocina, durante una hora o hasta que la masa duplique su volumen. El tiempo dependerá de la temperatura ambiente, en invierno puede necesitar algo más.
  8) Enciende el horno a 240º, dejando dentro tres bandejas para que se vayan calentando.
  9) Ya sea preparada de forma manual o en panificadora, una vez haya sido amasada y fermentada, pasa la masa a la mesa enharinada, amasando unos segundos con los nudillos para eliminar el exceso de aire.
  10) Divide la masa en tres porciones. 
  11) Forma una bola con cada parte (bolear la masa), extendiendo con el rodillo hasta formar panes de forma alargada, planos, de unos 8 mm de grosor. 
  12) Engrasa ligeramente la superficie con aceite de girasol, y sigue estirando el resto de la masa de igual forma.
  13) Coloca los naan sobre las bandejas calientes, espolvorea con los cominos y el cilantro picado, y hornea a 240º unos 8 minutos, o hasta que suban y queden esponjosos. Luego pasa bajo el grill unos segundos, o hasta que queden bien dorados. Unta con mantequilla derretida y sirve al momento, calientes.

* **Fuente**:
  * http://www.pepekitchen.com/articulo/pan-naan-con-yogurt-y-ajo-receta-paso-a-paso/
 
  <a href="#top">Volver</a>

## Empanada de Atún

Receta para prepara la empanada de atún en le Thermomix.

* **Ingredientes**:
  * 300 g de cebolla
  * 150 g de pimiento rojo
  * 150 g de pimiento verde
  * 200 g de tomate triturado (orlando triturado que compra NES)
  * 50 g de aceite de oliva virgen extra
  * 1 cucharadita de azúcar
  * 1 cucharadita de sal
  * 200 g de bonito en escabeche (2 latas y media)
  * 2 huevos cocidos
  * 1 paquete de 2 laminas de masa de hojaldre 
 
* **Elaboración**: 
  1) Precalentar el horno a 200ºC (arriba y abajo).
  2) Ponga en el vaso la cebolla, el pimiento (rojo y verde), el tomate, el aceite, el azúcar y la sal. Trocee programando 4 segundos, velocidad 5. Con la espátula, baje los restos de verduras de la tapa y del interior del vaso hasta las cuchillas.
  3) Programe 20 minutos, temperatura Varoma, velocidad 1 1/2.
  4) Retire a un recipiente y añada el bonito, escurrido y desmenuzado, y los huevos cocidos troceados. Dejar enfriar.
  5) Extender una de las láminas de la masa de hojaldre. Poner el relleno y cubrir con la segunda lámina de hojaldre. Atención: dejar unos 2 centimetro por el borde de la masa de hojaldre para poder cerrar con la ayuda de un tenedor.
  6) Con la yema de 2 huevos, pintar con la ayuda de una brocha todo el hojaldre.
  7) Hornear durante 30 minutos.

  <a href="#top">Volver</a>

## Alcachofas

* **Elaboración**:
  1) Vacío el frasco en el que vienen las alcachofas. 
  2) Aclaro las alcachofas sobre un colador para quitar el sabor que tengan del conservate por si le diera algún sabor.
  3) En un cazo o cazuela pones un poco de aceite, más o menos una cucharada pasadita, que cubra un poco el fondo del cazo.
  4) Pochar cebolla: yo suelo hacer con cebolleta porque me paece más suave y lo vas moviendo hasta obscurecer un poco esa cebolla.
  5) Antes tienes preparada la maizena dedo y medio a dos dedos de agua con la punta de cucharilla de maizena revolver.Normalente esa mezcla decanta, es decir, se separa el agua de la maizena.Yo suelo echar una puntia de pastilla caldo de carne Knorr ó de lo contrario un chorrito pequeño de soja líquida, una de las dos cosas. Las dos no porqe quedaría demasiado salado.
  6) Después échale un chorrito de vino blanco si teneis sino nada pero le da un toqe bueno.
  7) Y por último se echan las alcachofas remover un poco con toda la salsilla y dejar tapado.
  8) Finalmente, trocearle el jamón, darle una vuelta con la cuchara y lo retiras.
 
  **Advertencia**: Sólamente advertirte que si lo haces a una hora y lo vas a comer p.e. al de otra hora no le eches el jamón porque se va salando mientras lo tengas en el cazo mejor momentos antes de ir a comerlo.

  <a href="#top">Volver</a>

# POSTRES

## Brownie de chocolate

**Tiempo elaboración**: 15 minutos

* **Ingredientes**:
  * 150 gr. Chocolate (yo he utilizado chocolate negro)
  * 125 gr. Mantequilla
  * 300 gr. de Nueces (más o menos unas 25-30 nueces)
  * 1 cucharada pequeña de levadura
  * 1 vaso de harina
  * 1 vaso de azúcar
  * 4 huevos

* **Elaboración**:
  1) Verter los 150 gr. de chocolate y los 125 gr. de mantequilla en el “Tupper” y taparlo con la tapa del propio “Tupper”. Meterlo al microondas durante 3 minutos.
  2) Tras sacarlo del microondas, mientre se remueve la anterior mezcla, añadir la harina, el azúcar, los huevos, las nueces y la levadura hasta conseguir una buena masa mezclada.
  3) Introducir de nuevo el “Tupper” tapado en el microondas durante 6 minutos.
  4) A comer!!

  <a href="#top">Volver</a>
  
## Tarta de 3 chocolates

**Tiempo elaboración**: 1h

* **Ingredientes**:
  * 1 paquete de galletas María ( o el tipo de galletas que quieras para poner en la base)
  * Mantequilla (o margarina) – Lo suficiente como para hacer una pasta con el paquete de galletas
  * 200 gr. de chocolate negro
  * 200 gr. de chocolate con leche
  * 200 gr. de chocolate blanco
  * 3 sobres de cuajada (a utilizar uno por cada chocolate)

* **Elaboración**:
  1) Se tritura el paquete de galletas y se junta con la mantequilla hasta conseguir la “pasta” que forma la base. 
  2) Por otro lado, se empieza fundiendo los 200 gr. de chocolate negro. 
  3) Mientras se está fundiendo el chocolate negro, se prepara un vaso de leche al que se le añade un sobre de cuajada. 
  4) Cuando se ha diluido bien el sobre de cuajada y la leche, se verte el vaso de leche sobre el cazo donde se está fundiendo el chocolate. 
  5) En cuanto se consiga que el chocolate este espeso, se echa sobre la base de galletas. 
  6) Se mete en la nevera a enfriar mientras se prepara el siguiente nivel de la tarta. 
  7) A continuación se repito el mismo proceso con el chocolate con leche. Hay que tener cuidado a la hora de echar el chocolate con leche sobre el anterior nivel de chocolate negro, ya que si cae de golpe podrían entremezclarse. Para ello, es recomendable ayudarse de una cuchara grande para que no caiga de golpe sobre el anterior nivel. 
  8) Finalmente se repetirá el mismo proceso con el chocolate blanco. Meterlo en la nevera dejándolo unas cuantas horas (lo mejor es hacerlo de un día para otro).

  <a href="#top">Volver</a>
  
## Donuts

Receta original de Donuts.

* **Ingredientes para la masa**:
  * Harina 310 gr
  * Azucar 100 gr
  * Levadura de Panaderia 5 gr
  * Sal
  * Mantequilla 70 gr
  * Leche 50 gr
  * 2 Huevos
  * 1 Limon

* **Elaboración**:
  1) Hacer la masa y dejar reposar 1 hora.
  2) Con la harina reservada, poner sobre la mesa donde pondremos la masa para aplastarla, sin llegar a aplastarla demasiado (dejarlo un poco gordo).
  3) Azúcar glasse + limon + agua

  <a href="#top">Volver</a>

## Tarta de chocolate de Izeko

* **Ingredientes**:
  * Galletas rectangulares
  * 1 litro Leche (para hacer el chocolate)
  * Un poco más de leche en un cazo para remojar las galletas
  * Azucar (para la leche que se utiliza para remojar las galletas)
  * 1 paquete de Cacao Valor de medio kilo (para hacer chocolate a la taza)
  * Brandy, un chorrito (para echar en la leche)
  * Maizena (una cuchara rasa que se echa en un poco de leche fría)

* **Elaboración**:
  1) Empezamos haciendo el chocolate en la thermomix. 
  2) Echamos el paquete de cacao, la leche y la maizena disuelta (en un poco de leche aparte) en la Thermomix durante 7 minutos, 90º y velocidad 4-5. 
  3) Una vez terminado, lo ponemos en un cuenco aparte para ir cubriendo cada piso de la tarta.
  4) En la base de la tarta, pondremos una poquito de mantequilla (para evitar que se pegue). En un recipiente aparte ponemos la leche(fría o natural, nunca caliente) y el brandy. 
  5) A continuación empezaremos a empapar las galletas sobre la leche e ir realizando los diferentes pisos de la tarta. 
  6) Al final de cada piso, cubriremos con una capa de chocolate. Realizaremos esta operación tantas veces como pisos queramos en la tarta (aproximadamente, 5 pisos).

  <a href="#top">Volver</a>
